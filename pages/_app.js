import { useState, useEffect } from 'react';
import NavBar from '../components/NavBar';

import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import { UserProvider } from '../UserContext';

function MyApp({ Component, pageProps }) {

	const [user, setUser] = useState({
		email: null,
		isAdmin: null
	})

	    useEffect(() => {
        console.log(user.email);
    }, [user.email])


	const unsetUser = () => {
		localStorage.clear();
	}

  return(
  	<UserProvider value={{user, setUser, unsetUser}}>
	  	<NavBar/>
	  	<Component {...pageProps} />
  	</UserProvider>
  )
}

export default MyApp
