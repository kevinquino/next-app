import { useContext } from 'react';
import coursesData from '../../data/courses';
import CourseCard from '../../components/CourseCard';
import { Table, Button, Container } from 'react-bootstrap';
import UserContext from '../../UserContext';
import Head from 'next/head'

export default function index(){
	//use the UserContext and destructure it to access the user state declared in _app
	const { user } = useContext(UserContext)

	const courses = coursesData.map(courseData => {
		return(
			<CourseCard key={courseData.id} courseProp={courseData}/>
		)
	})

	const courseRows = coursesData.map(courseData => {
		return(
			<tr key={courseData.id}>
				<td>{courseData.id}</td>
				<td>{courseData.name}</td>
				<td>Php {courseData.price}</td>
				<td>{courseData.onOffer ? 'open' : 'closed'}</td>
				<td>{courseData.start_date}</td>
				<td>{courseData.end_date}</td>
				<td>
					<Button className="bg-warning">Update</Button>
					<Button className="bg-danger">Disable</Button>
				</td>
			</tr>
		)
	})

	return(
		user.isAdmin === true
		?
		<Container>
			<Head>
				<title>Courses Admin Dashboard</title>
			</Head>
			<h1>Course Dashboard</h1>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Price</th>
						<th>Status</th>
						<th>Start Date</th>
						<th>End Date</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{courseRows}
				</tbody>
			</Table>
		</Container>
		:
		<Container>
			<Head>
				<title>Courses</title>
			</Head>
			{courses}
		</Container>
	)
}	